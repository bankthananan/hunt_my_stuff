
import {
  createStackNavigator,
  createAppContainer,
} from 'react-navigation'
// import the different screens
import Loading from './app/screens/authen/Loading'
import SignUp from './app/screens/authen/SignUp'
import Login from './app/screens/authen/Login'
import MainTabNavigator from './app/navigation/MainTabNavigator'

// create our app's navigation stack
const App = createStackNavigator(
  {
    Loading,
    SignUp,
    Login,
    MainTabNavigator,
 
  },
  {
    initialRouteName: 'Loading',
    mode:'modal',
    headerMode:'none'
  }
)

export default createAppContainer(App);
