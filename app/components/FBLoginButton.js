import React from 'react';
import { TouchableHighlight, Button, TouchableOpacity, View, Text } from 'react-native';
import * as Expo from 'expo';
import { f, auth, database, storage } from '../../app/config/firebaseConfig'
import { SocialIcon } from 'react-native-elements'

export default class FBLoginButton extends React.Component {
    loginWithFacebook = async () => {
        const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
            '640705706356055',
            { permissions: ['email', 'public_profile'] }
        );
        if (type === 'success') {
            const credentials = f.auth.FacebookAuthProvider.credential(token);
            f
                .auth().signInWithCredential(credentials)
            f.auth().onAuthStateChanged(function (user) {

                if (user) {
                    console.log(user)
                    database.ref('user').child(user.uid).child('name').set(user.displayName)
                    database.ref('user').child(user.uid).child('username').set(user.email)
                    database.ref('user').child(user.uid).child('email').set(user.email)
                    database.ref('user').child(user.uid).child('phoneNumber').once('value').then(function (snapshot) {
                        if (snapshot.val() <= 0) {
                            database.ref('user').child(user.uid).child('phoneNumber').set('')
                        }
                    }).catch(error => console.log(error))
                    database.ref('user').child(user.uid).child('avatar').set(user.photoURL)
                    database.ref('user').child(user.uid).child('exp').once('value').then(function (snapshot) {
                        console.log('Snap ', snapshot.val())
                        if (snapshot.val() <= 0) {
                            database.ref('user').child(user.uid).child('exp').set(0)
                        }
                    }).catch(error => console.log(error))

                    database.ref('user').child(user.uid).child('role').set('user')
                }
            })
            .then(() => this.props.navigation.navigate('Loading'))
                .catch((error) => {
                    console.log("Error ", error)
                })
        }
    }


    // this.props.navigation.navigate('MainTabNavigator')

    // this.props.navigation.navigate('EditProfile')





    render() {
        return (


            <TouchableOpacity onPress={() => this.loginWithFacebook()}>
                {/* <View style={{
                    backgroundColor: '#4267B2', alignItems: 'center',
                    justifyContent: 'center', borderRadius: 15,
                    width: 200, height: 50
                }}
                >
                   
                  
                </View> */}
                <SocialIcon
                    onPress={() => this.loginWithFacebook()}
                    title='Log in with Facebook'
                    button
                    type='facebook'
                    style={{ width: 200 }}
                />
            </TouchableOpacity>

        );
    };
};

module.exports = FBLoginButton;