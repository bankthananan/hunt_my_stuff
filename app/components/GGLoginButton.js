import React from 'react';
import { TouchableHighlight, Button, TouchableOpacity, View, Text, Image } from 'react-native';
import * as Expo from 'expo';
import { f, auth, database, storage } from '../../app/config/firebaseConfig'
import { SocialIcon } from 'react-native-elements'
// import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
export default class GGLoginButton extends React.Component {
  loginWithGoogle = async () => {

    const { type, accessToken } = await Expo.Google.logInAsync({
      androidClientId: '1027890205985-fpmec05jot1iojp50jgcdaeco0dhkdhk.apps.googleusercontent.com',
      scopes: ['profile', 'email'],
    });

    if (type === 'success') {
      const credentials = f.auth.GoogleAuthProvider.credential(null, accessToken);
      f
        .auth().signInWithCredential(credentials)
      f.auth().onAuthStateChanged(function (user) {
        if (user) {
          database.ref('user').child(user.uid).child('name').set(user.displayName)
          database.ref('user').child(user.uid).child('username').set(user.email)
          database.ref('user').child(user.uid).child('email').set(user.email)
          database.ref('user').child(user.uid).child('phoneNumber').once('value').then(function (snapshot) {
            console.log('Snap ', snapshot.val())
            if (snapshot.val() <= 0) {
              database.ref('user').child(user.uid).child('phoneNumber').set('')
            }
          }).catch(error => console.log(error))

          database.ref('user').child(user.uid).child('avatar').set(user.photoURL)
          database.ref('user').child(user.uid).child('exp').once('value').then(function (snapshot) {
            console.log('Snap ', snapshot.val())
            if (snapshot.val() <= 0) {
              database.ref('user').child(user.uid).child('exp').set(0)
            }
          }).catch(error => console.log(error))
          database.ref('user').child(user.uid).child('role').set('user')
        }
      })
      .then(() => this.props.navigation.navigate('Loading'))
        .catch((error) => {
          console.log("Error ", error)
        })
    }
  }
  render() {
    return (

      <TouchableOpacity onPress={() => this.loginWithGoogle()}>
        <View style={{
          backgroundColor: 'white', alignItems: 'center',
          justifyContent: 'center', borderRadius: 30,
          width: 200, height: 50, flexDirection: 'row'
        }}>
          <Image style={{ width: 25, height: 25 }} source={require('../assets/images/google_icon.png')}></Image>
          <Text style={{ color: 'black', fontSize: 15 }}>  Log in with Google</Text>
        </View>
      </TouchableOpacity>


    );
  };
};

module.exports = GGLoginButton;