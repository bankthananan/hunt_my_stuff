import React, {Fragment} from 'react'
import { TouchableOpacity, FlatList, Styesheet, Text, View, Image } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js'
import ViewMoreText from 'react-native-view-more-text';
import Comment from '../screens/comments'


class PhotoList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            photo_feed: [],
            refresh: false,
            loading: true,
            userId: '',
            empty: false,
            curUser: ''
        }
    }
    checkAuthState() {
        var that = this
        f.auth().onAuthStateChanged(function (user) {
            if (user) {
                that.setState({
                    curUser: user.uid,
                })
            }

        })
    }
    componentDidMount = () => {
        const { isUser, userId } = this.props;
        this.checkAuthState()
        if (isUser) {
            this.loadFeed(userId)
            this.setState({
                isUser: isUser
            })
            // console.log(isUser, userId)
        } else {

            this.loadFeed('')
        }
    }

    pluralCheck = (s) => {
        if (s == 1) {
            return 'ago';
        } else {
            return 's ago'
        }
    }

    timeConverter = (timestamp) => {
        var a = new Date(timestamp * 1000);
        var secounds = Math.floor((new Date() - a) / 1000)
        var interval = Math.floor(secounds / 31536000)
        if (interval > 1) {
            return interval + ' year' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 2592000)
        if (interval > 1) {
            return interval + ' month' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 86400)
        if (interval > 1) {
            return interval + ' day' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 3600)
        if (interval > 1) {
            return interval + ' hour' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 60)
        if (interval > 1) {
            return interval + ' minute' + this.pluralCheck(interval);
        }
        return Math.floor(secounds) + ' second' + this.pluralCheck(interval)
    }

    addToFlatList = (photo_feed, data, photo, userId) => {
        var that = this;
        var photoObj = data[photo]
        database.ref('user').child(photoObj.author).child('username').once('value').then((snapshot) =>{
            const exists = (snapshot.val() !== null);
            if (exists) data = snapshot.val();
            photo_feed.push({
                id: photo,
                url: photoObj.url,
                caption: photoObj.caption,
                length: photoObj.posted,
                posted: that.timeConverter(photoObj.posted),
                author: data,
                authorId: photoObj.author,
                postName: photoObj.postName,
                des: photoObj.caption,
                price: photoObj.price,
            })
           
            //console.log('curUser ',this.state.curUser)
            var newPhotoFeed = photo_feed.slice(0);
            newPhotoFeed.sort(function (a, b) {
                return b.length - a.length;
            });
            this.setState({
                photo_feed: newPhotoFeed
            });
            // console.log('photo_feed ',photo_feed)
            // that.setState({
            //     refresh: false,
            //     loading: false,
            //     userId: userId,

            // })

        }).catch(error => console.log(error));
    }

    loadFeed = (userId = '') => {
        this.setState({
            refresh: true,
            photo_feed: []
        })
        var that = this;

        var loadRef = database.ref('photos')
        if (userId != '') {
            console.log('user id exisst')
            loadRef = database.ref('user').child(userId).child('photos')
        }
        loadRef.orderByChild('posted').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null);
            if (exists) {
                data = snapshot.val();
                var photo_feed = that.state.photo_feed;
                for (var photo in data) {
                    that.addToFlatList(photo_feed, data, photo, userId)
                }
            } else {
                that.setState({
                    empty: true,    
                })
            }
            that.setState({
                refresh: false,
                loading: false,
                userId: userId,

            })


        }).catch(error => console.log(error));
    }

    componentWillReceiveProps = (nextState) => {
        if (nextState.photo_feed != this.state.photo_feed) {
            // if(this.state.isUser){
            //     this.loadFeed(this.props.userId);
            // }
            // //   this.loadNew();
            this.setState({
                photo_feed: nextState.photo_feed
            })
            // this.componentDidMount();
            //     }
        }

    }
    // componentWillUnmount = () => {
    //     this.state.photo_feed = [''];
    //     // this.componentDidMount();
    // }
    loadNew = () => {
        if (this.state.isUser) {
            console.log('this.state.isUser ', this.state.isUser)
            this.loadFeed(this.props.userId);
        } else {
            this.loadFeed();
        }

    }
    renderViewMore(onPress) {
        return (
            <Text style={{ color: 'blue' }} onPress={onPress}>View more</Text>
        )
    }
    renderViewLess(onPress) {
        return (
            <Text style={{ color: 'blue' }} onPress={onPress}>View less</Text>
        )
    }
    // componentWillUnmount() {
    //     this.state = [''];
    //     // this.componentDidMount();
    // }
    render() {
        return (
            <View style={{ flex: 1 }}>

                {this.state.loading ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        {this.state.empty == true ? (
                            <View onRefresh={this.loadNew}>
                                <Text>No post</Text>
                            </View>
                        ) : (
                                <View onRefresh={this.loadNew}>
                                    <Text>Loading... </Text>
                                </View>
                            )}
                    </View>
                ) : (
                        <FlatList refreshing={this.state.refresh}
                            onRefresh={this.loadNew}
                            data={this.state.photo_feed}
                            keyExtractor={(item, index) => index.toString()}
                            style={{ flex: 1, backgroundColor: '#eee' }}
                            renderItem={({ item, index }) => (
                                <View key={index} style={{ width: '100%', overflow: 'hidden', justifyContent: 'space-between' }}>
                                    <View style={{ padding: 5, width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#ffc4c6' }}>

                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('User', { userId: item.authorId })}>
                                            <Text>{item.author}</Text>
                                        </TouchableOpacity>
                                        <Text style={{ opacity: 75 }}>{item.posted}</Text>
                                    </View>
                                    <View>
                                        <Image source={{ uri: item.url }}
                                            style={{ resizeMode: 'cover', width: '100%', height: 250 }} />
                                    </View>
                                    <View style={{ padding: 5, backgroundColor: '#ff999c' }}>
                                        <View style={{ flexDirection: 'row', width:'100%'}}>

                                            <Text style={{ fontWeight: 'bold' }}>Topic:</Text>
                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.navigate('Comment', { photoId: item.id, curUser: this.state.curUser })}
                                            >
                                                <ViewMoreText
                                                    numberOfLines={3}
                                                    renderViewMore={this.renderViewMore}
                                                    renderViewLess={this.renderViewLess}
                                                >
                                                    <Text style={{width:'100%'}}> {item.postName}</Text>
                                                </ViewMoreText>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignSelf:'stretch'}}>

                                            <Text style={{ fontWeight: 'bold', padding: 1, position:'absolute' }}>Description:</Text>
                                            <ViewMoreText
                                                numberOfLines={3}
                                                renderViewMore={this.renderViewMore}
                                                renderViewLess={this.renderViewLess}
                                            >
                                                <Text style={{ width: '100%' }}>{item.des}</Text>
                                            </ViewMoreText>
                                            </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontWeight: 'bold' }}>Price  :</Text>
                                            <Text > {item.price}</Text>
                                        </View>

                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Comment', { photoId: item.id, curUser: this.state.curUser })}>
                                            <Text style={{ marginTop: 10, textAlign: 'center', backgroundColor: '#F55E61', borderWidth: 1.5, borderRadius: 20 }}>Read more</Text>
                                        </TouchableOpacity>
                                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Chat', { photoId: item.id, curUser: this.state.curUser })}>
                                            <Text style={{ marginTop: 10, textAlign: 'center', backgroundColor: '#F55E61', borderWidth: 1.5, borderRadius: 20 }}>Chat</Text>
                                        </TouchableOpacity> */}
                                    </View>
                                </View>

                            )
                            }
                        />
                    )}
            </View >
        )
    }

}

export default PhotoList;