import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBXzxZRKG-b0uSOYxNJIX3ZwdArP2prI2g",
  authDomain: "hunt-my-stuff-efdba.firebaseapp.com",
  databaseURL: "https://hunt-my-stuff-efdba.firebaseio.com",
  projectId: "hunt-my-stuff-efdba",
  storageBucket: "hunt-my-stuff-efdba.appspot.com",
  messagingSenderId: "1027890205985",
  appId: "1:1027890205985:web:6d1773b8b71149ae"
};
firebase.initializeApp(firebaseConfig)
uploadImage = async uri => {
  console.log('got image to upload. uri:' + uri);
  try {
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase
      .storage()
      .ref('avatar')
      .child(uuid.v4());
    const task = ref.put(blob);

    return new Promise((resolve, reject) => {
      task.on(
        'state_changed',
        () => { },

      );
    });
  } catch (err) {
    console.log('uploadImage try/catch error: ' + err.message);
  }
};

updateAvatar = url => {

  var userf = firebase.auth().currentUser;
  if (userf != null) {
    userf.updateProfile({ avatar: url }).then(
      function () {
        console.log('Updated avatar successfully. url:' + url);
        alert('Avatar image is saved successfully.');
      },
      function (error) {
        console.warn('Error update avatar.');
        alert('Error update avatar. Error:' + error.message);
      }
    );
  } else {
    console.log("can't update avatar, user is not login.");
    alert('Unable to update avatar. You must login first.');
  }
};


export const f = firebase;
export const database = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();

