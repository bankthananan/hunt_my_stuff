import React from 'react'
import { TouchableOpacity, FlatList, Styesheet, Text, View, Image ,ImageBackground} from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js'
import { grey } from 'ansi-colors';
import PhotoList from '../components/photoList.js'



class userProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,

        }
    }
    checkParams = () => {
        var params = this.props.navigation.state.params;

        if (params) {
            if (params.userId) {

                this.setState({
                    userId: params.userId
                })
                this.fetchUserInfo(params.userId);
            }
        }
    }

    fetchUserInfo = (userId) => {
        var that = this;


        database.ref('user').child(userId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) data = snapshot.val();
            that.setState({
                username: data.username,
                name: data.name,
                avatar: data.avatar,
                loggedin: true,
                userId: userId,
                phoneNumber: data.phoneNumber,
                email: data.email,
                loading: false
            })

        }).catch(error => console.log(error))
    }

    componentDidMount = () => {
        this.checkParams();
    }

    componentWillUnmount=()=>{
        this.state = {
            loaded: false,
            username: '',
            name: '',
            avatar: '',
            loggedin: false,
            userId: '',
            phoneNumber: '',
            email: '',
            loading: false

        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', borderBottomWidth: 0.5, justifyContent: 'space-between', alignItems: 'center' }}>
                    <View style={{ width: '80%', padding: 5, flexDirection: 'row', justifyContent: 'space-between', marginRight: 30 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Text>Go back</Text>
                        </TouchableOpacity>
                        <Text>Profile</Text>
                        <Text></Text>
                    </View>
                </View>
                <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flexDirection: 'row', backgroundColor: '#DB393D' }}>
                    {
                        this.state.avatar ? (
                            <View>

                                {this.state.exp >= 1000 ? (
                                    <ImageBackground source={require('../assets/images/frame1.png')} style={{ width: 100, height: 100 }}>
                                        <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 23.5, marginTop: 19 }} source={{ uri: this.state.avatar }} />
                                    </ImageBackground>
                                ) : (this.state.exp >= 100 ?
                                    <ImageBackground source={require('../assets/images/frame2.png')} style={{ width: 75, height: 75 }}>
                                        <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 10, marginTop: 10 }} source={{ uri: this.state.avatar }} />
                                    </ImageBackground>
                                    : <ImageBackground source={require('../assets/images/frame3.png')} style={{ width: 60, height: 60 }}>
                                        <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 2.3, marginTop: 2.3 }} source={{ uri: this.state.avatar }} />
                                    </ImageBackground>)}
                            </View>
                        ) : (<View>

                            {this.state.exp >= 1000 ? (
                                <ImageBackground source={require('../assets/images/frame1.png')} style={{ width: 100, height: 100 }}>

                                </ImageBackground>
                            ) : (this.state.exp >= 100 ?
                                <ImageBackground source={require('../assets/images/frame2.png')} style={{ width: 75, height: 75 }}>

                                </ImageBackground>
                                : <ImageBackground source={require('../assets/images/frame3.png')} style={{ width: 60, height: 60 }}>

                                </ImageBackground>)}
                        </View>
                            )}

                    <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flexDirection: 'row', paddingVertical: 10 }}>
                        <View style={{ marginHorizontal: 20 }}>
                            <Text style={{ color: 'white' }}>{this.state.name}</Text>
                            <Text style={{ color: 'white' }}>{this.state.username}</Text>
                            <Text style={{ color: 'white' }}>{this.state.phoneNumber}</Text>
                            <Text style={{ color: 'white' }}>{this.state.email}</Text>
                        </View>
                    </View>


                </View>
                {this.state.userId ?
                    (<PhotoList isUser={true} userId={this.state.userId} navigation={this.props.navigation} />) : null
                }
            </View>



        )
    }

}


export default userProfile;