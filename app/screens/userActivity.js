import React from 'react';
import { ActivityIndicator, TextInput, TouchableOpacity, FlatList, Styesheet, Text, View, Image } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import UserAuth from '../screens/authen/SignUp'
import ViewMoreText from 'react-native-view-more-text';

class userActivity extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            empty: false,
            refresh: false,
            photo_feed: [],
            userId: ''
        }
    }
    componentDidMount = () => {
        this.checkAuth()
    }
    checkAuth = () => {
        f.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({
                    curUser: user.uid
                })
                this.fetchUserPhoto()
            }
        })
    }

    fetchUserPhoto = () => {
        console.log(this.state.curUser)
        loadRef = database.ref('user').child(this.state.curUser).child('photos')
        loadRef.orderByChild('posted').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null);
            if (exists) {
                data = snapshot.val();
                var photo_feed = this.state.photo_feed;
                for (var photo in data) {
                    this.addToFlatList(photo_feed, data, photo)
                }
            } else {
                this.setState({
                    empty: true,
                })
            }
            this.setState({
                refresh: false,
                loading: false,

            })


        }).catch(error => console.log(error));
    }
    addToFlatList = (photo_feed, data, photo,) => {
        console.log('in add list')
        var that = this;
        var photoObj = data[photo]
        database.ref('user').child(photoObj.author).child('username').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null);
            if (exists) {
                console.log(data)
                data = snapshot.val();
                if (data.requester != '') {
                    photo_feed.push({
                        id: photo,
                        url: photoObj.url,
                        caption: photoObj.caption,
                        length: photoObj.posted,
                        author: data,
                        authorId: photoObj.author,
                        postName: photoObj.postName,
                        des: photoObj.caption,
                        price: photoObj.price,
                    })
                }
            }
            //console.log('curUser ',this.state.curUser)
            var newPhotoFeed = photo_feed.slice(0);
            newPhotoFeed.sort(function (a, b) {
                return b.length - a.length;
            });
            this.setState({
                photo_feed: newPhotoFeed
            });

        }).catch(error => console.log(error));
    }
    render() {
        return (
            <View>
                <Text>hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh</Text>
                <FlatList
                    data={this.state.photo_feed}
                    renderItem={({ item }) => (
                        <View>
                            <View>
                                <Image source={{ uri: item.url }}
                                    style={{ marginTop: 5, resizeMode: 'cover', width: 50, height: 50 }} />
                            </View>

                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Comment', { photoId: item.id, curUser: this.state.curUser })}>
                                <ViewMoreText
                                    numberOfLines={3}
                                    renderViewMore={this.renderViewMore}
                                    renderViewLess={this.renderViewLess}
                                ><Text style={{ marginTop: 10 }}>Post name: {item.caption}</Text>
                                </ViewMoreText>
                            </TouchableOpacity>
                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                            >
                                <Text>{item.postName}</Text>
                            </ViewMoreText>


                        </View>
                    )}
                ></FlatList>
            </View>
        )
    }
}
export default userActivity