import React from 'react'
import { TouchableOpacity, FlatList, Styesheet, Text, View, Image, TextInput, ImageBackground } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import { grey } from 'ansi-colors';

class editProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,

        }
    }
    checkParams = () => {
        var params = this.props.navigation.state.params;

        if (params) {
            if (params.userId) {
                this.setState({
                    userId: params.userId
                })
                this.fetchUserInfo(params.userId);
            }
        }
    }

    fetchUserInfo = (userId) => {
        console.log("edit profil userID is: ", userId)
        var that = this;
        database.ref('user').child(userId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) data = snapshot.val();
            that.setState({
                exp: data.exp
            })
        }).catch(error => console.log(error))
        database.ref('user').child(userId).child('username').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val();

                console.log("username", data)
                that.setState({ username: data })
            } else {
                console.log("NO data")
                that.setState({ username: '' })
            }
        }).catch(error => console.log(error))

        database.ref('user').child(userId).child('name').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val();
                console.log("name", data)
                that.setState({ name: data })
            } else {
                that.setState({ name: '' })
            }
        }).catch(error => console.log(error))

        database.ref('user').child(userId).child('avatar').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val();
                console.log("avatar", data)

                that.setState({ avatar: data })
            } else {
                that.setState({ avatar: '' })
            }
        }).catch(error => console.log(error))

        database.ref('user').child(userId).child('phoneNumber').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val();
                console.log("phoneNumber", data)

                that.setState({ phoneNumber: data })
            } else {
                that.setState({ phoneNumber: '' })
            }
        }).catch(error => console.log(error))
        database.ref('user').child(userId).child('email').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val();
                console.log("email", data)

                that.setState({ email: data })
            } else {
                that.setState({ email: '' })
            }
        }).catch(error => console.log(error))
    }


    componentDidMount = () => {
        this.checkParams();
    }
    saveProfile = () => {

        var name = this.state.name
        var username = this.state.username
        var phoneNumber = this.state.phoneNumber
        var email = this.state.email
        var avatar = this.state.avatar
        console.log("avatar", avatar)
        if (name !== '' && username !== '' && phoneNumber !== '' && email !== '') {
            database.ref('user').child(this.state.userId).child('name').set(name)
            database.ref('user').child(this.state.userId).child('username').set(username)
            database.ref('user').child(this.state.userId).child('phoneNumber').set(phoneNumber)
            // database.ref('user').child(this.state.userId).child('avatar').set('http://www.gravatar.com/avatar')
            database.ref('user').child(this.state.userId).child('email').set(email)
            this.props.navigation.navigate('Profile')
        } else {
            alert("Please fill all forms")
        }

    }



    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                        <Text>Edit Profile</Text>
                        <Text> </Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', backgroundColor: '#DB393D' }}>
                    {
                        this.state.avatar ?
                        <View>

                        {this.state.exp >= 1000 ? (
                            <ImageBackground source={require('../assets/images/frame1.png')} style={{ width: 100, height: 100 }}>
                                <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 23.5, marginTop: 19 }} source={{ uri: this.state.avatar }} />
                            </ImageBackground>
                        ) : (this.state.exp >= 100 ?
                            <ImageBackground source={require('../assets/images/frame2.png')} style={{ width: 75, height: 75 }}>
                                <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 10, marginTop: 10 }} source={{ uri: this.state.avatar }} />
                            </ImageBackground>
                            : <ImageBackground source={require('../assets/images/frame3.png')} style={{ width: 60, height: 60 }}>
                                <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 2.3, marginTop: 2.3 }} source={{ uri: this.state.avatar }} />
                            </ImageBackground>)}
                    </View>
                            : <View>
                                <ImageBackground source={require('../assets/images/frame1.png')} style={{ width: 100, height: 100 }}></ImageBackground>
                            </View>
                    }

                    <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flexDirection: 'row', paddingVertical: 10 }}>
                        <View style={{ marginRight: 10 }}>
                            <Text style={{ color: 'white' }}>{this.state.name}</Text>
                            <Text style={{ color: 'white' }}>{this.state.username}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ backgroundColor: '#ff999c', height: '100%' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ width: 80, color: 'white', marginVertical: 15, padding: 5 }}>Name - Surname:</Text>
                        <TextInput
                            editable={true}
                            placeholder={'Name - Surname'}
                            onChangeText={(text) => this.setState({ name: text })}
                            value={this.state.name}
                            style={{ color: 'white', width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderWidth: 1 }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ width: 80, color: 'white', marginVertical: 15, padding: 5 }}>Username:</Text>
                        <TextInput
                            editable={true}
                            placeholder={'Username'}
                            onChangeText={(text) => this.setState({ username: text })}
                            value={this.state.username}
                            style={{ color: 'white', width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderWidth: 1 }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ width: 80, color: 'white', marginVertical: 15, padding: 5 }}>Phone number:</Text>
                        <TextInput
                            editable={true}
                            placeholder={'Phone number'}
                            onChangeText={(text) => this.setState({ phoneNumber: text })}
                            keyboardType={'numeric'}
                            value={this.state.phoneNumber}
                            style={{ color: 'white', width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderWidth: 1 }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ width: 80, color: 'white', marginVertical: 15, padding: 5 }}>Email:</Text>
                        <TextInput
                            editable={true}
                            placeholder={'Email'}
                            onChangeText={(text) => this.setState({ email: text })}
                            value={this.state.email}
                            style={{ color: 'white', width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderWidth: 1 }}
                        />
                    </View>
                    <TouchableOpacity
                        onPress={() => this.saveProfile()}
                        style={{ marginTop: 10, marginHorizontal: 40, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D' }}>
                        <Text style={{ textAlign: 'center', color: 'white' }}>Save</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ marginTop: 10, marginHorizontal: 40, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D' }}>
                        <Text style={{ textAlign: 'center', color: 'white' }}>Cancel</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }

};

export default editProfile;