import React from 'react'
import { StyleSheet, Text, TextInput, View, Button, TouchableHighlight, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Expo from 'expo';
import { f, auth, database, storage } from '../../config/firebaseConfig'
var FBLoginButton = require('../../components/FBLoginButton');
var GGLoginButton = require('../../components/GGLoginButton');
export default class Login extends React.Component {
  state = { email: '', password: '', errorMessage: null }

  loginUser = async (email, password) => {
    if (email != '' & password != '') {
      f
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(f.auth().onAuthStateChanged(user => { console.log('user ', user) }))
        .then(() => this.props.navigation.navigate('Loading'))
        .catch(function (error) {
          var errorCode = error.code;
          var errorMessage = error.message;
          if (errorCode == 'auth/wrong-password') {
            alert('wrong password');
          } else if (errorCode == 'auth/invalid-email') {
            alert('email is not correct');

          } else if (errorCode == 'auth/user-not-found') {
            alert("email doesn't exit in the database");
          } else {
            alert(errorMessage);
          }
          console.log(error);
        });

    } else {
      alert('Please fill the forms');
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Image style={{ width: 200, height: 200 }} source={require('../../assets/images/logo.png')}></Image>
        <Text></Text>
        <View>
          <Text style={{ color: 'white', fontSize: 15 }}>Email :                                   </Text>
          <TextInput

            style={styles.textInput}
            onChangeText={(text) => this.setState({ email: text })}
            value={this.state.email}
          />
          <Text style={{ color: 'white', fontSize: 15 }}>Password :                                </Text>
          <TextInput

            style={styles.textInput}
            onChangeText={(text) => this.setState({ pass: text })}
            secureTextEntry={true}
            value={this.state.pass}
          />
        </View>
        <View>
          <Text></Text>
          <Text></Text>
        </View>
        <View style={{ flexDirection: 'row',  justifyContent: 'center',alignItems: 'center', }}>
         
       
          <TouchableOpacity
             onPress={() => this.loginUser(this.state.email, this.state.pass)}>
            <Text style={{ height: 30, width: 100,color: 'green', backgroundColor: 'white', borderRadius: 15, textAlign: 'center',paddingTop: 5,fontSize:15 }}>Login</Text>
          </TouchableOpacity>
             <Text>        </Text>
          <TouchableOpacity
              onPress={() => this.props.navigation.navigate('SignUp')}>
            <Text style={{ height: 30, width: 100,color: 'blue', backgroundColor: 'white', borderRadius: 15, textAlign: 'center' ,paddingTop: 5,fontSize:15}}>Register</Text>
          </TouchableOpacity>
      
        </View>
        <Text></Text>
        <FBLoginButton />
        <Text></Text>
        <GGLoginButton />
        <View></View>

      </View>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F55E61'
  },
  textInput: {
    height: 40,
    width: 250,
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8,
    backgroundColor: 'white',
    borderRadius: 15,
    paddingLeft: 10
  },
  bottonAc: {
    backgroundColor: 'white',
  }
})
