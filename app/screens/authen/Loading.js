import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet , Image} from 'react-native'
import { f, auth, database, storage } from '../../config/firebaseConfig'
export default class Loading extends React.Component {

    componentDidMount() {
        this.checkAuthState();
    }
    checkAuthState(){
        f.auth().onAuthStateChanged(user => {
            this.props.navigation.navigate(user ? 'MainTabNavigator' : 'Login')
          })
    }
    render() {
        return (
            <View style={styles.container}>
                <Image style={{width:100,height:100}} source={require('../../assets/images/logo.png')}></Image>
                <Text style={{ fontSize: 15, color: 'white' }}>Loading...</Text>
                <ActivityIndicator size="large" />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F55E61'
    },
})
