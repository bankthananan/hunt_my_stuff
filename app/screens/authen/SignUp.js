import React from 'react';
import { ActivityIndicator, TextInput, TouchableOpacity, KeyboardAvoidingView, FlatList, Styesheet, Text, View, Image } from 'react-native'
import { f, auth, database, storage } from '../../config/firebaseConfig';


class UserAuth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            authStep: 0,
            email: '',
            pass: '',
            name: '',
            username: '',
            phoneNumber: '',
            moveScreen: false
        }

    }


    componentDidMount = () => {
        if (this.props.moveScreen == true) {
            this.setState({ moveScreen: true })
        }
    }

    createUserObj = (userObj, email) => {
        console.log('user information are', userObj)
        var uObj = {
            name: this.state.name,
            username: this.state.username,
            phoneNumber: this.state.phoneNumber,
            avatar: 'http://www.gravatar.com/avatar',
            email: email,
            exp:0,
            role:'user'
        }
        database.ref('user').child(userObj.uid).set(uObj)
    }
    signUp = async () => {
        var email = this.state.email
        var pass = this.state.pass
        var name = this.state.name
        var username = this.state.username
        var phoneNumber = this.state.phoneNumber

        if (email != '' && pass != ''&& name!=''&&username!=''&&phoneNumber!='') {
            try {
                let user = await auth.createUserWithEmailAndPassword(email, pass)
                .then((userObj) => this.createUserObj(userObj.user, email))  //, name, username, phoneNumber
                    .catch(function (error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        if (errorCode == 'auth/weak-password') {
                            alert('Password should be more 6 charecters');
                        } else if (errorCode == 'auth/email-already-in-use') {
                            alert("email is already in used ");
                        } else if (errorCode == 'auth/invalid-email') {
                            alert('email is not correct');
                        }
                        else {
                            alert(errorMessage);
                        }
                        console.log(error);
                    });
            } catch{
                console.log(error)
                alert(error)
            }
        } else {
            alert('Please fill all form')
        }
    }

    render() {
        // const { navigate } = this.props.navigation;
        var that = this
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' ,  backgroundColor: '#F55E61'}}>

                <Text>{this.props.message}</Text>
                <View style={{ marginVertical: 20 }}>

                    <View>

                        <Text style={{ fontWeight: 'bold', marginBottom: 20 }}>Register</Text>

                        <Text>Name - Surname:</Text>
                        <TextInput
                            editable={true}
                            secureTextEntry={false}
                            placeholder={'Input your name'}
                            onChangeText={(text) => this.setState({ name: text })}
                            value={this.state.name}
                            style={{ width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderRadius: 3, borderWidth: 1 , backgroundColor: 'white'}}
                        />

                        <Text>Email:</Text>
                        <TextInput
                            editable={true}
                            keyboardType={'email-address'}
                            placeholder={'Input your email'}
                            onChangeText={(t) => this.setState({ email: t })}
                            value={this.state.email}
                            style={{ width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderRadius: 3, borderWidth: 1, backgroundColor: 'white' }}
                        />

                        <Text>Username:</Text>
                        <TextInput
                            editable={true}
                            secureTextEntry={false}
                            placeholder={'Input your username'}
                            onChangeText={(text) => this.setState({ username: text })}
                            value={this.state.username}
                            style={{ width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderRadius: 3, borderWidth: 1, backgroundColor: 'white' }}
                        />

                        <Text>Password :</Text>
                        <TextInput
                            editable={true}
                            secureTextEntry={true}
                            placeholder={'Input your password'}
                            onChangeText={(text) => this.setState({ pass: text })}
                            value={this.state.pass}
                            style={{ width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderRadius: 3, borderWidth: 1, backgroundColor: 'white' }}
                        />


                        <Text>Tel number:</Text>
                        <TextInput
                            editable={true}
                            secureTextEntry={false}
                            placeholder={'Input your tel number'}
                            onChangeText={(phone) => this.setState({ phoneNumber: phone })}
                            value={this.state.phoneNumber}
                            style={{ width: 250, marginVertical: 10, padding: 5, borderColor: 'grey', borderRadius: 3, borderWidth: 1, backgroundColor: 'white' }}
                        />
                        <Text></Text>
                        <TouchableOpacity
                            style={{ backgroundColor: 'blue', paddingVertical: 10, paddingHorizontal: 20, borderRadius: 5 }}
                            onPress={() => this.signUp()}
                        >
                            <Text style={{ color: 'white' }}>Register</Text>
                        </TouchableOpacity>
                        <Text></Text>
                        <Text></Text>

                        <TouchableOpacity
                            style={{ backgroundColor: 'blue', paddingVertical: 10, paddingHorizontal: 20, borderRadius: 5 }}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Text style={{ color: 'white' }}>Cancel</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        )
    }
}
export default UserAuth;