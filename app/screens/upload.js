import React from 'react';
import { ActivityIndicator, TextInput, TouchableOpacity, FlatList, Styesheet, Text, View, Image } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
//import { ImagePicker, Permissions } from 'expo'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import { PermissionsAndroid } from 'react-native';
import UserAuth from '../screens/authen/SignUp'


class upload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedin: false,
      imageId: this.uniqueId(),
      imageSelected: false,
      uploading: false,
      caption: "",
      price: '',
      postName: '',
      progress: 0
    }
  }

  _checkPermissions = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ camera: status });

    const { statusRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({ cameraRoll: statusRoll });
  };

  s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };
  uniqueId = () => {
    return (
      this.s4() + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4()
    );
  };

  findNewImage = async () => {
    this._checkPermissions();

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: "Images",
      allowsEditing: true,
      quality: 1
    });

    console.log(result);

    if (!result.cancelled) {
      console.log("upload image");
      this.setState({
        imageSelected: true,
        imageId: this.uniqueId(),
        uri: result.uri
      });
      //this.uploadImage(result.uri);
    } else {
      console.log("cancel");
      this.setState({
        imageSelected: false
      });
    }
  };

  uploadPublish = () => {

    if (this.state.uploading == false) {
      if (this.state.caption != "" && this.state.postName != "" && this.state.price != "") {
        //
        this.uploadImage(this.state.uri);
      } else {
        alert("Please fill all forms");
      }
    } else {
      console.log("Ignore button tap as already uploading");
    }

  };

  uploadImage = async uri => {
    //
    var that = this;
    var userid = f.auth().currentUser.uid;
    var imageId = this.state.imageId;

    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(uri)[1];
    this.setState({
      currentFileType: ext,
      uploading: true
    });

    var FilePath = imageId + "." + that.state.currentFileType;

    const oReq = new XMLHttpRequest();
    oReq.open("GET", uri, true);
    oReq.responseType = "blob";
    oReq.onload = () => {
      const blob = oReq.response;
      //Call function to complete upload with the new blob to handle the uploadTask.
      this.completeUploadBlob(blob, FilePath);
    };
    oReq.send();


  };

  completeUploadBlob = (blob, FilePath) => {
    var that = this;
    var userid = f.auth().currentUser.uid;
    var imageId = this.state.imageId;

    var uploadTask = storage
      .ref("user/" + userid + "/img")
      .child(FilePath)
      .put(blob);

    uploadTask.on(
      "state_changed",
      function (snapshot) {
        var progress = (
          (snapshot.bytesTransferred / snapshot.totalBytes) *
          100
        ).toFixed(0);
        console.log("Upload is " + progress + "% complete");
        that.setState({
          progress: progress
        });
      },
      function (error) {
        console.log("error with upload - " + error);
      },
      function () {
        //complete
        that.setState({ progress: 100 });
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          console.log(downloadURL);
          that.processUpload(downloadURL);
        });
      }
    );
  };

  processUpload = imageUrl => {
    //Process here...

    //Set needed info
    var imageId = this.state.imageId;
    var userId = f.auth().currentUser.uid;
    var caption = this.state.caption;
    var dateTime = Date.now();
    var timestamp = Math.floor(dateTime / 1000);
    var price = this.state.price
    var postName = this.state.postName
    //Build photo object
    //author, caption, posted, url

    var photoObj = {
      author: userId,
      caption: caption,
      price: price,
      posted: timestamp,
      postName: postName,
      url: imageUrl
    };

    //Update database

    //Add to main feed
    database.ref("/photos/" + imageId).set(photoObj);

    //Set user photos object
    database.ref("/user/" + userId + "/photos/" + imageId).set(photoObj);

    alert("Posted Successfully!");

    this.setState({
      uploading: false,
      imageSelected: false,
      caption: "",
      price: '',
      postName: '',
      uri: ""
    });
    this.props.navigation.navigate('Feed')
  };

  componentDidMount = () => {
    var that = this;
    f.auth().onAuthStateChanged(function (user) {
      if (user) {
        //Logged in
        that.setState({
          loggedin: true
        });
      } else {
        //Not logged in
        that.setState({
          loggedin: false
        });
      }
    });
  };

  render() {
    return (

      <View style={{ flex: 1 }}>
        <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <View></View>
          <Text>Upload </Text>
          <View></View>
        </View>
        {this.state.loggedin == true ? (
          <View style={{ flex: 1 }}>
            <View></View>
            <View>
            
              <TouchableOpacity style={{ marginTop:10,alignSelf: 'center', width: 170, marginHorizontal: 'auto',paddingVertical: 10, backgroundColor: '#DB393D', borderRadius: 5, }}
                onPress={() => this.findNewImage()}>
                <Text style={{ textAlign: 'center', color: 'white' }}>Select image</Text>
              </TouchableOpacity>
              <Image source={{ uri: this.state.uri }} style={{ marginTop: 10, resizeMode: 'cover', width: 125, height: 125,alignSelf: 'center' }} />
            </View>
            <View></View>
            <View styl={{ padding: 5 }}>
              <Text style={{ marginTop: 5 }}>Topic:</Text>

              <TextInput
                editable={true}
                placeholder={'Fill topic here'}
                maxLength={50}
                multipleLine={4}
                numberOfLine={4}
                onChangeText={(text) => this.setState({ postName: text })}
                style={{ marginVertical: 10, height: 50, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
              />
              <Text style={{ marginTop: 5 }}>Description</Text>
              <TextInput
                editable={true}
                placeholder={'Fill description here'}
                maxLength={150}
                multipleLine={4}
                numberOfLine={4}
                onChangeText={(text) => this.setState({ caption: text })}
                style={{ marginVertical: 10, height: 100, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
              />
              <Text style={{ marginTop: 5 }}>Price</Text>

              <TextInput
                editable={true}
                placeholder={'Fill price here'}
                maxLength={16}
                multipleLine={4}
                numberOfLine={4}
                keyboardType={'numeric'}
                onChangeText={(text) => this.setState({ price: text })}
                style={{ marginVertical: 10, height: 50, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
              />

              <TouchableOpacity
                onPress={() => this.uploadPublish()}
                style={{ alignSelf: 'center', width: 170, marginHorizontal: 'auto', backgroundColor: '#DB393D', borderRadius: 5, paddingVertical: 10, paddingHorizontal: 20 }}>
                <Text style={{ textAlign: 'center', color: 'white' }}>Upload post</Text>
              </TouchableOpacity>
              {this.state.uploading == true ? (
                <View style={{ marginTop: 10 }}>
                  <Text>Upload Process</Text>
                  <Text>{this.state.progress}</Text>

                  {this.state.progress != 100 ? (
                    <ActivityIndicator size="small" color="blue" />
                  ) : (
                      <Text>Processing...</Text>
                    )}
                </View>
              ) : (
                  <View></View>
                )}

            </View>
            {/* {this.state.imageSelected == true ? (
              <View style={{ flex: 1 }}>
                <View style={{ height: 70, paddingTop: 30, backgroundColor: 'white', borderColor: 'lightgrey', borderBottomWidth: 0.5, justifyContent: 'center', alignItems: 'center' }}>
                  <Text>New post</Text>
                </View>
                <View styl={{ padding: 5 }}>
                  <Text style={{ marginTop: 5 }}>Topic:</Text>

                  <TextInput
                    editable={true}
                    placeholder={'Fill topic here'}
                    maxLength={50}
                    multipleLine={4}
                    numberOfLine={4}
                    onChangeText={(text) => this.setState({ postName: text })}
                    style={{ marginVertical: 10, height: 50, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
                  />
                  <Text style={{ marginTop: 5 }}>Description</Text>
                  <TextInput
                    editable={true}
                    placeholder={'Fill description here'}
                    maxLength={150}
                    multipleLine={4}
                    numberOfLine={4}
                    onChangeText={(text) => this.setState({ caption: text })}
                    style={{ marginVertical: 10, height: 100, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
                  />
                  <Text style={{ marginTop: 5 }}>Price</Text>

                  <TextInput
                    editable={true}
                    placeholder={'Fill price here'}
                    maxLength={16}
                    multipleLine={4}
                    numberOfLine={4}
                    keyboardType={'numeric'}
                    onChangeText={(text) => this.setState({ price: text })}
                    style={{ marginVertical: 10, height: 50, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
                  />

                  <TouchableOpacity
                    onPress={() => this.uploadPublish()}
                    style={{ alignSelf: 'center', width: 170, marginHorizontal: 'auto', backgroundColor: 'purple', borderRadius: 5, paddingVertical: 10, paddingHorizontal: 20 }}>
                    <Text style={{ textAlign: 'center', color: 'white' }}>Upload post</Text>
                  </TouchableOpacity>
                  {this.state.uploading == true ? (
                    <View style={{ marginTop: 10 }}>
                      <Text>Upload Process</Text>
                      <Text>{this.state.progress}</Text>

                      {this.state.progress != 100 ? (
                        <ActivityIndicator size="small" color="blue" />
                      ) : (
                          <Text>Processing...</Text>
                        )}
                    </View>
                  ) : (
                      <View></View>
                    )}
                  <Image source={{ uri: this.state.uri }} style={{ marginTop: 10, resizeMode: 'cover', width: '100%', height: 275 }} />
                </View>
              </View>
            ) : (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 28, paddingBottom: 15 }}>New post</Text>
                  <TouchableOpacity style={{ paddingVertical: 10, paddingHorizontal: 20, backgroundColor: '#DB393D', borderRadius: 5 }}
                    onPress={() => this.findNewImage()}>
                    <Text>Select image</Text>
                  </TouchableOpacity>

                </View>
              )} */}
          </View>

        ) : (
            <UserAuth message={' please login to Upload'} />

          )}
      </View>



    )
  }
}
export default upload;