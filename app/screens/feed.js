import React from 'react';
import { TouchableOpacity, FlatList, Styesheet, Text, View, Image } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import PhotoList from '../components/photoList.js';

class feed extends React.Component {

    render() {
        return (
            <View style={{flex:1}}>
            <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', borderBottomWidth: 0.5, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Feed</Text>
            </View>
            
            <PhotoList isUser={false} navigation={this.props.navigation} />
            </View>

        )
    }

}
export default feed;