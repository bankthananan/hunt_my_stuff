import React from 'react';
import { FlatList, Styesheet, Text, View, Image, TextInput, Keyboard, ActivityIndicator } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import Icon from 'react-native-vector-icons/Ionicons'
import * as animatable from 'react-native-animatable'
import { ListItem, SearchBar } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ViewMoreText from 'react-native-view-more-text';

class searchElas extends React.Component {
    serchState = {
        searchBarFocus: false
    }
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            refresh: false,
            loading: true,
            dirtybox: false,
            wordList: '',
            photo_feed: [],
            spamPhoto: [],
            respondInState: [],
            firstPhotoList:[]
        }
    }
    componentDidMount = () => {
        this.fetchpostAtFirst()
        this.setState({
            loading: false
        })
        this.makeRemoteRequest()

    }
    getSuggestedWord = (text) => {
        this.setState({
            dirtybox: true,
            refresh: true,
            loading: true,
            spamPhoto: []
        })
        return fetch('http://huntelastic.herokuapp.com/search=' + text).then((response) => response.json())
            .then((responseJson) => {
                if(responseJson!=''){
                this.setState({
                    respondInState: responseJson
                })
                console.log('responseJson in state', this.state.respondInState);
                var wordList = this.state.wordList
                var counter = 0
                for (inwordList in responseJson) {
                    console.log('inwordList', responseJson[counter], 'conter is:', counter)
                    this.fetchPost(this.state.respondInState[counter])
                    counter++
                }
                return responseJson
            }else{
                this.setState({
                    dirtybox: false,
                    loading:false
                })
                alert('No post found ')
            }
            }).catch((error) => {
                console.error(error);
            });
    }

    fetchPost = (photoId) => {
        this.setState({
            refresh: true,
            loading: true,
        })
        console.log('in state spam', this.state.spamPhoto)
        database.ref('photos').child(photoId).once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null);
            if (exists) {
                data = snapshot.val();
                console.log('data', data)
                var spamPhotoList = this.state.spamPhoto;
                spamPhotoList.push({
                    caption: data.postName,//ทำไมมันต้องสลับกันว่ะ
                    postName: data.caption,
                    id: photoId,
                    price: data.price,
                    url: data.url,
                })
            }
            console.log('spamlist', spamPhotoList)
            this.setState({
                loading: false,
                spamPhoto: spamPhotoList
            })
            //console.log('photolist', spamPhotoList)

        }).catch(error => console.log(error));

    }

    fetchpostAtFirst = () => {
        database.ref('photos').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null);
            console.log('snap',snapshot.val())
            if (exists) data = snapshot.val();
            var spamPhotoList = this.state.firstPhotoList;
            for (var photo in data) {
              this.addPostToFlatList(spamPhotoList, data, photo)
            }
            this.setState({
              loadList: true,
              refresh: false
            })
    
          }).catch(error => console.log(error));
    }

    addPostToFlatList = (spamPhotoList, data, photo) => {
        var photoObj = data[photo]
        spamPhotoList.push({
          caption: photoObj.postName,//ทำไมมันต้องสลับกันว่ะ
          postName: photoObj.caption,
          id: photo,
          price: photoObj.price,
          url: photoObj.url,
        })
        this.setState({
            firstPhotoList: spamPhotoList
        })
    
      }
      makeRemoteRequest = () => {
        const url = `https://randomuser.me/api/?&results=20`;
        this.setState({ loading: true });
        //console.log(url)
        fetch(url)
          .then(res => res.json())
          .then(res => {
            this.setState({
              data: this.state.firstPhotoList,
              error: res.error || null,
              loading: false,
            });
    
            this.arrayholder = this.state.firstPhotoList;//res.results
            //console.log("In make remote", this.arrayholder)
    
          })
          .catch(error => {
            this.setState({ error});
          });
      };
    

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <View>
                <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View></View>
                    <Text>Search </Text>
                    <View></View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, width: '80%' }}
                        placeholder="Type Here..."
                        onChangeText={(text) => this.setState({ searchText: text })}
                    ></TextInput>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.getSuggestedWord(this.state.searchText)}>
                            <Text>Search</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    { this.state.dirtybox ? (
                    <FlatList
                        data={this.state.spamPhoto}
                        renderItem={({ item }) => (
                            <View>
                                <View>
                                    <Image source={{ uri: item.url }}
                                        style={{ marginTop: 5, resizeMode: 'cover', width: 50, height: 50 }} />
                                </View>

                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Comment', { photoId: item.id, curUser: this.state.curUser })}>
                                    <ViewMoreText
                                        numberOfLines={3}
                                        renderViewMore={this.renderViewMore}
                                        renderViewLess={this.renderViewLess}
                                    ><Text style={{ marginTop: 10 }}>Post name: {item.caption}</Text>
                                    </ViewMoreText>
                                </TouchableOpacity>
                                <ViewMoreText
                                    numberOfLines={3}
                                    renderViewMore={this.renderViewMore}
                                    renderViewLess={this.renderViewLess}
                                >
                                    <Text>{item.postName}</Text>
                                </ViewMoreText>


                            </View>
                        )}
                    ></FlatList>
                    ):(
                        <FlatList
                        data={this.state.firstPhotoList}
                        renderItem={({ item }) => (
                            <View>
                                <View>
                                    <Image source={{ uri: item.url }}
                                        style={{ marginTop: 5, resizeMode: 'cover', width: 50, height: 50 }} />
                                </View>

                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Comment', { photoId: item.id, curUser: this.state.curUser })}>
                                    <ViewMoreText
                                        numberOfLines={3}
                                        renderViewMore={this.renderViewMore}
                                        renderViewLess={this.renderViewLess}
                                    ><Text style={{ marginTop: 10 }}>Post name: {item.caption}</Text>
                                    </ViewMoreText>
                                </TouchableOpacity>
                                <ViewMoreText
                                    numberOfLines={3}
                                    renderViewMore={this.renderViewMore}
                                    renderViewLess={this.renderViewLess}
                                >
                                    <Text>{item.postName}</Text>
                                </ViewMoreText>


                            </View>
                        )}
                    ></FlatList>
                    )}
                </View>

            </View>
        );
    }
}
export default searchElas