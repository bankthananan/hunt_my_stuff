import React from 'react';
import { FlatList, Styesheet, Text, View, Image, Alert } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Comments from './comments.js';
import AdminCheck from './adminCheck.js';

class activity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedin: true,
            // editingProfile: false,
            loading: true,
            type: '',
            requester: [],
            userrequestList: [],
            isReqesut: false,
            requester_list: [],
            curUser: '',
            refresh: false,
            curRequesterId: '',
            isAdmin: false

        }
    }
    checkParams = () => {
        var params = this.props.navigation.state.params;
        if (params) {
            if (params.photoId) {
                this.setState({
                    photoId: params.photoId,
                    postName: params.postName
                })
                this.fetchReqester(params.photoId)
            }

        }
    }
    fetchUserInfo = (userId) => {
        var that = this;
        database.ref('user').child(userId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) data = snapshot.val();
            that.setState({
                username: data.username,
                name: data.name,
                avatar: data.avatar,
                loggedin: true,
                userId: userId,
                type: 'user',
                role: data.role
            })
            console.log("role in fetch user", that.state.role)
            if (that.state.role == "admin") {
                that.setState({
                    isAdmin: true
                })
            }
            console.log("User role", that.state.isAdmin)
        }).catch(error => console.log(error))
    }
    componentDidMount = () => {
        var that = this;

        f.auth().onAuthStateChanged(function (user) {
            if (user) {
                that.setState({
                    curUser: user.uid
                })
                that.fetchUserInfo(that.state.curUser)
                that.checkParams()

            }
        })

    }

    pluralCheck = (s) => {
        if (s == 1) {
            return ' ago';
        } else {
            return 's ago'
        }
    }

    timeConverter = (timestamp) => {
        var a = new Date(timestamp * 1000);
        var secounds = Math.floor((new Date() - a) / 1000)
        var interval = Math.floor(secounds / 31536000)
        if (interval > 1) {
            return interval + ' year' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 2592000)
        if (interval > 1) {
            return interval + ' month' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 86400)
        if (interval > 1) {
            return interval + ' day' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 3600)
        if (interval > 1) {
            return interval + ' hour' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 60)
        if (interval > 1) {
            return interval + ' minute' + this.pluralCheck(interval);
        }
        return Math.floor(secounds) + ' scond' + this.pluralCheck(interval)
    }

    fetchReqester = (photoId) => {
        this.setState({
            refresh: true,
            requester: []
        })
        database.ref('photos').child(photoId).child('requester').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val()
                var requester = this.state.requester
                for (var request in data) {
                    this.addTolist(requester, data, request)
                }
                var newRequest = requester
                this.setState({
                    requester: newRequest
                })
            } else {
                console.log("nothing in the page")
                this.setState({
                    refresh: false,
                    loading: false,
                    isReqesut: false
                })
            }
        }).catch(error => console.log(error));
    }

    loadFeed = (userId) => {
        console.log("Id in load feed", userId)
        var that = this;
        console.log("photoId is:", that.state.photoId)
        database.ref('user').child(userId).child('photos').child(that.state.photoId).child('requester').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null);
            if (exists) {
                data = snapshot.val();
                var requester_list = this.state.requester_list;
                for (var request in data) {
                    that.addTolist(requester_list, data, request)
                }

                this.setState({
                    refresh: false,
                    loading: false,
                    isReqesut: true
                })
            }


        }).catch(error => console.log(error));
    }
    addTolist = (requester, data, request) => {
        var req = request
        var that = this
        var requestObj = data[request]
        console.log("requestObj", requestObj)
        if (requestObj != '') {
            database.ref('user').child(requestObj).once('value').then((snapshot) => {
                const exists = (snapshot.val() !== null);
                if (exists) data = snapshot.val();
                requester.push({
                    id: requestObj,
                    username: data.username,
                    name: data.name,
                    avatar: data.avatar,
                    phoneNumber: data.phoneNumber,
                    email: data.email,
                    loggedin: true,
                    curRequesterId: req
                })
                console.log(that.state.avatar)
                var newRequester = requester.slice(0);
                newRequester.sort(function (a, b) {
                    return b.length - a.length;
                });

                this.setState({
                    requester: newRequester,
                    loading: false,
                    isReqesut: true,
                    refresh: false
                });
                // console.log("request in add", this.state.requester_list)
            })
        } else {
            console.log("nothing in the page")
            this.setState({
                refresh: false,
                loading: false,
                isReqesut: false
            })
        }
    }
    componentWillUnmount() {
        this.state = [''];
        // this.componentDidMount();
    }

    addToFlatList = (photo_feed, data, photo, userId) => {
        var that = this;
        var photoObj = data[photo]
        database.ref('user').child(photoObj.author).child('username').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null);
            if (exists) data = snapshot.val();
            photo_feed.push({
                id: photo,
                url: photoObj.url,
                caption: photoObj.caption,
                length: photoObj.posted,
                posted: that.timeConverter(photoObj.posted),
                author: data,
                authorId: photoObj.author,
                postName: photoObj.postName
            })
            console.log("requessster in reqeust", photo_feed)
            var newPhotoFeed = photo_feed.slice(0);
            newPhotoFeed.sort(function (a, b) {
                return b.length - a.length;
            });
            that.setState({
                photo_feed: newPhotoFeed
            });
            that.setState({
                refresh: false,
                loading: false,
                userId: userId
            })
        }).catch(error => console.log(error));
    }
    loadNew = () => {
        this.fetchReqester(this.state.photoId)
    }
    onAccept = (passedUser) => {
        var partnerId = Comments.shared.uniqueId();
        // loadRef = database.ref('user').child(userId).child('photos')

        database.ref('/photos/' + this.state.photoId + '/requester/' + passedUser.curReId).remove()
        console.log("this.state.photoId ", this.state.photoId)
        console.log("passedUser.curReId ", passedUser.curReId)
        database.ref('/photos/' + this.state.photoId + '/partner/' + partnerId).set(passedUser.userId).then(() => this.loadNew())
        console.log("partnerId ", partnerId)
        console.log("passedUser.userId ", passedUser.userId)
        alert("You have accepted this request")
    }
    onDecline = (passedUser) => {
        database.ref('photos/' + this.state.photoId + '/requester/' + passedUser.curReId).remove().then(() => this.loadNew())
        console.log("this.state.photoId ", this.state.photoId)
        console.log("passedUser.curReId ", passedUser.curReId)
        alert("You have declined this request")
    }
    componentWillReceiveProps = (nextState) => {
        if (nextState.requester_list != this.state.requester_list) {
            this.loadNew()
        }

    }
    render() {
        console.log("Role in render ", this.state.isAdmin)
        if (!this.state.isAdmin) {
            return (
                <View style={{ flex: 1 }}>
                    <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View></View>
                        <Text>Activity </Text>
                        <View></View>
                    </View>
                    {this.state.loading ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 100 }}>
                            {this.state.empty == true ? (
                                <View><Text>No request found</Text></View>
                            ) : (
                                    <View>
                                        <Text>Loading... </Text>
                                    </View>
                                )}
                        </View>
                    ) : (
                        <userActivity navigation={this.props.navigation} />
                        )}
                </View>
            )
        } else {
            return (
                <AdminCheck navigation={this.props.navigation} />)

        }
    }



}
export default activity;