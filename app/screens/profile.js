import React from 'react';
import { TouchableOpacity, FlatList, Styesheet, Text, View, Image, TextInput, ImageBackground } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import { grey } from 'ansi-colors';
import PhotoList from '../components/photoList.js';
import UserAuth from '../screens/authen/SignUp'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
class profile extends React.Component {
    _menu = null;
    constructor(props) {
        super(props);
        this.state = {
            loggedin: true,
            editingProfile: false,
            loading: true,
            isLoaded: false

        }

    }
    checkParams = () => {
        var params = this.props.navigation.state.params;

        if (params) {
            if (params.userId) {
                console.log(params.userId)
                this.setState({
                    userId: params.userId,
                    loading: false
                })
                this.fetchUserInfo(params.userId);
            }
        } else {
            this.setState({
                empty: true
            })
        }
    }

    fetchUserInfo = (userId) => {
        var that = this;
        database.ref('user').child(userId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) data = snapshot.val();
            that.setState({
                username: data.username,
                name: data.name,
                avatar: data.avatar,
                loggedin: true,
                userId: userId,
                phoneNumber: data.phoneNumber,
                email: data.email,
                loading: false,
                exp: data.exp
            })
        }).catch(error => console.log(error))


    }
    checkAuthState() {
        var that = this;
        f.auth().onAuthStateChanged(function (user) {

            if (user) {
                that.fetchUserInfo(user.uid);
            } else {
                that.setState({
                    loggedin: false,
                    isLoaded: false
                })
            }

        })
    }
    componentDidMount = () => {
        this.checkParams()
        this.checkAuthState()
    }
    componentWillReceiveProps = (nextState) => {
        if (nextState.name != this.state.name) {
            this.checkParams();
        }

    }
    logoutUser = () => {
        var that = this;
        that.setState({
            loggedin: false,
        })
        this._menu.hide();
        f.auth().signOut()
            .then(() => this.props.navigation.navigate('Loading'))
        alert('Logout successfully')
    }
    setMenuRef = ref => {
        this._menu = ref;
    };
    showMenu = () => {
        this._menu.show();
    };
    hideMenu = () => {
        this._menu.hide();
    };
    goEdit = () => {
        this._menu.hide();
        this.props.navigation.navigate('EditProfile', { userId: this.state.userId })
    }
    componentWillUnmount() {
        this.state = [''];
        // this.componentDidMount();
    }



    render() {
        console.log(this.state.loading)
        console.log(this.state.exp)
        return (
            <View refreshing={true} style={{ flex: 1 }}>
                {this.state.loading == true ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        {/* <Text>No posts found</Text> */}
                        {this.state.isLoaded == true ? (
                            <View>
                                <Text>No post </Text>
                            </View>
                        ) : (
                                <View>
                                    <Text>No posts found</Text>
                                </View>
                            )}

                    </View>
                ) : (
                        <View >
                            <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <View></View>
                                <Text>Profile </Text>
                                <View style={{}}>
                                    <Menu
                                        ref={this.setMenuRef}

                                        button={
                                            <TouchableOpacity onPress={this.showMenu}>
                                                <Image
                                                    source={require('../assets/images/menu1.png')}
                                                    style={{ width: 30, height: 30, right: 0 }}

                                                />
                                            </TouchableOpacity>


                                        }>
                                        <MenuItem onPress={() => this.goEdit()}  >Edit Profile </MenuItem>
                                        {/* <MenuItem onPress={() => this.goUpload()} >Upload</MenuItem> */}
                                        <MenuItem onPress={() => this.logoutUser()} >Logout </MenuItem>
                                    </Menu>
                                </View>
                            </View>


                            <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flexDirection: 'row', backgroundColor: '#DB393D' }}>
                                {
                                    this.state.avatar ? (
                                        <View>

                                            {this.state.exp >= 1000 ? (
                                                <ImageBackground source={require('../assets/images/frame1.png')} style={{ width: 100, height: 100 }}>
                                                    <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 23.5, marginTop: 19 }} source={{ uri: this.state.avatar }} />
                                                </ImageBackground>
                                            ) : (this.state.exp >= 100 ?
                                                <ImageBackground source={require('../assets/images/frame2.png')} style={{ width: 75, height: 75 }}>
                                                    <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 10, marginTop: 10 }} source={{ uri: this.state.avatar }} />
                                                </ImageBackground>
                                                : <ImageBackground source={require('../assets/images/frame3.png')} style={{ width: 60, height: 60 }}>
                                                    <Image style={{ width: 55, height: 55, borderRadius: 75, marginLeft: 2.3, marginTop: 2.3 }} source={{ uri: this.state.avatar }} />
                                                </ImageBackground>)}
                                        </View>
                                    ) : (<View>

                                        {this.state.exp >= 1000 ? (
                                            <ImageBackground source={require('../assets/images/frame1.png')} style={{ width: 100, height: 100 }}>

                                            </ImageBackground>
                                        ) : (this.state.exp >= 100 ?
                                            <ImageBackground source={require('../assets/images/frame2.png')} style={{ width: 75, height: 75 }}>

                                            </ImageBackground>
                                            : <ImageBackground source={require('../assets/images/frame3.png')} style={{ width: 60, height: 60 }}>

                                            </ImageBackground>)}
                                    </View>
                                        )}

                                <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flexDirection: 'row', paddingVertical: 10 }}>
                                    <View style={{ marginHorizontal: 20 }}>
                                        <Text style={{ color: 'white' }}>{this.state.name}</Text>
                                        <Text style={{ color: 'white' }}>{this.state.username}</Text>
                                        <Text style={{ color: 'white' }}>{this.state.phoneNumber}</Text>
                                        <Text style={{ color: 'white' }}>{this.state.email}</Text>
                                    </View>
                                </View>


                            </View>


                        </View>
                    )}

                {
                    this.state.userId ?
                        (<PhotoList isUser={true} userId={this.state.userId} navigation={this.props.navigation} />) : (null)
                }



            </View>



        )
    }
}


export default profile;