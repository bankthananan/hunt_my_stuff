import React from 'react'
import { GiftedChat } from 'react-native-gifted-chat'
import { f, auth, database, storage } from '../config/firebaseConfig'
import firebaseSvc from '../config/FirebaseSvc'
import NavigationBar from "react-native-navbar";
import { Platform, Text, KeyboardAvoidingView } from 'react-native';
import { View } from 'react-native-animatable';
import KeyboardSpacer from 'react-native-keyboard-spacer';



export default class Chat extends React.Component {


  static navigationOptions = ({ navigation }) => ({
    title: (navigation.state.params || {}).name || 'Chat!',
  });

  state = {
    messages: [],
  };

  get user() {
    return {
      name: firebaseSvc.curName,
      email: firebaseSvc.email,
      avatar: firebaseSvc.avatar,
      _id: firebaseSvc.uid,

    };
  }


  componentDidMount() {
    // if(that.state.isLogin===true){
    firebaseSvc.on(message =>
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, message),
      }))
      , this.props.navigation.state.params.photoId);
    console.log('this.state.messages ', this.state.messages)
    // }



  }
  componentWillUnmount() {
    firebaseSvc.off();
  }
  goBack = ()=>{
    this.props.navigation.goBack()
  }

  render() {
    const rightButtonConfig = {
      // title: 'Add photo',

    };
    const leftButtonConfig = {
      title: 'Back',
      handler: () => this.goBack()

    };

    return (

      <View style={{ flex: 1 }}>
         <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey'}}>

          <NavigationBar
          title={{ title: "Chat" }}
          rightButton={rightButtonConfig}
          leftButton={leftButtonConfig}
        />
         
        </View>
       
        <GiftedChat
          messages={this.state.messages}
          onSend={firebaseSvc.send}
          user={this.user}
          isAnimated={true}
        />
        <KeyboardSpacer
          topSpacing={23} />
      </View>

    )

  }


}
