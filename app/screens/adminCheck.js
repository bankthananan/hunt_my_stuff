import React from 'react';
import { FlatList, Styesheet, Text, View, Image, Alert } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Comments from './comments.js';
import ViewMoreText from 'react-native-view-more-text';

class activity extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isAdmin: false,
            loading: true,
            isLoaded: false,
            spamPhoto: [],
            loadList: false,
            curUser: '',
            photoId: '',
            refresh: false,
            authorId: ''
        }
    }

    componentDidMount = () => {
        console.log("Com did in admincheck")

        this.checkUserState()
        this.fetchSpamPost()
    }

    checkUserState = () => {
        f.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({
                    curUser: user.uid,
                    spamPhoto: []
                })
                this.fetchUserInfo(user.uid)
            } else {
            }

        })
    }

    fetchUserInfo = (userId) => {
        database.ref('user').child(userId).once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val();
                if (data.role == 'admin') {
                    this.setState({
                        isAdmin: true,
                        loading: false,
                        isLoaded: true
                    })
                } else {
                    this.setState({
                        loading: false,
                        isLoaded: true
                    })
                }
            }

        }).catch(error => console.log(error))
    }

    fetchSpamPost = () => {
        database.ref('photos').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null);
            if (exists) data = snapshot.val();
            var spamPhotoList = this.state.spamPhoto;
            for (var photo in data) {
                this.addSpamToFlatList(spamPhotoList, data, photo)
            }
            this.setState({
                loadList: true,
                refresh: false
            })
        }).catch(error => console.log(error));
    }

    addSpamToFlatList = (spamPhotoList, data, photo) => {
        console.log("photo in add", photo)
        var photoObj = data[photo]
        if (photoObj.is_spam) {
            spamPhotoList.push({
                caption: photoObj.postName,//ทำไมมันต้องสลับกันว่ะ
                postName: photoObj.caption,
                id: photo,
                authorId: photoObj.author
            })
        }
        this.setState({
            spamPhoto: spamPhotoList
        })

    }

    setis_spamfalse = (photoId) => {
        console.log("Current user", this.state.curUser)
        console.log("Photo Id", this.state.photoId)
        Alert.alert(
            'Ignor',
            'Are you sure?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Accept', onPress: () => database.ref('photos').child(photoId).child('is_spam').set(false).then(this.loadNew()) },
            ],
            { cancelable: false }
        );
    }

    deletePost = (photoId, authorId) => {
        console.log("Current user", this.state.curUser)
        console.log("Photo Id", this.state.photoId)
        Alert.alert(
            'Delete post',
            'Delete this post?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Accept', onPress: () => database.ref('/photos/' + photoId).remove().then(database.ref('/user/' + authorId + '/photos/' + this.state.photoId).remove()).then(this.loadNew()) },
            ],
            { cancelable: false }
        );
        // database.ref('/photos/' + this.state.photoId).remove
    }

    loadNew = () => {
        this.componentDidMount()
        this.setState({
            refresh: true,
            isLoaded: false,
            loading: true,
            loadList: false,
            spamPhoto: []
        })
    }
    componentWillUnmount = () => {
        this.setState({
            isAdmin: false,
            loading: true,
            isLoaded: false,
            spamPhoto: [],
            loadList: false,
            curUser: '',
            photoId: '',
            refresh: false,
            authorId: ''
        })
    }

    render() {
        return (
            <View refreshing={true} >
                <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View></View>
                    <Text>Spam List </Text>
                    <View></View>
                </View>
                {this.state.isLoaded == true ? (
                    <View>
                        {this.state.isAdmin ? (
                            <View>

                                {this.state.loadList ? (


                                    <FlatList
                                        refreshing={this.state.refresh}
                                        data={this.state.spamPhoto}
                                        onRefresh={this.loadNew}
                                        keyExtractor={index => index.toString()}
                                        renderItem={({ item, index }) => (
                                            <View key={index} style={{ padding: 5, justifyContent: "space-between", borderRadius: 5, borderWidth: 2, backgroundColor: '#ffc4c6' }} >
                                                <View><Text style={{ fontSize: 18, color: 'red', backgroundColor: 'black' }}>SPAM POST</Text></View>
                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Comment', { photoId: item.id, curUser: this.state.curUser })}>
                                                    <Text style={{ marginTop: 10 }}>Post name: {item.caption}</Text>
                                                </TouchableOpacity>
                                                <ViewMoreText
                                                    numberOfLines={3}
                                                    renderViewMore={this.renderViewMore}
                                                    renderViewLess={this.renderViewLess}
                                                >
                                                    <Text>Caption: {item.postName}</Text>
                                                    </ViewMoreText>
                                                    <TouchableOpacity onPress={() => this.setis_spamfalse(item.id)} style={{ justifyContent: 'center' }}>
                                                        <Text style={{ marginTop: 10, backgroundColor: '#F55E61', width: 50, marginRight: 10 }}>Ignor</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.deletePost(item.id, item.authorId)} style={{ justifyContent: 'center' }}>
                                                        <Text style={{ marginTop: 10, backgroundColor: '#F55E61', width: 50 }}>Delete</Text>
                                                    </TouchableOpacity>
                                                    </View>
                                                )}
                                            />

                                    ) : (<Text>No spam post</Text>)}
                                </View>
                                        ) : (
                                        <View><Text>Not an admin</Text></View>
                                    )}

                            </View>
                        ) : (<Text>Loading</Text>)}
                    </View>
                )
    }



                }
export default activity;