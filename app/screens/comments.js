import React from 'react';
import { Alert, ScrollView, TextInput, TouchableOpacity, KeyboardAvoidingView, FlatList, Styesheet, Text, View, Image } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import { grey } from 'ansi-colors';
import UserAuth from '../screens/authen/SignUp'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import ViewMoreText from 'react-native-view-more-text';

class Comments extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loggedin: false,
            comments_list: [],
            photoId: '',
            photo: '',
            ownerId: '',
            owner: false,
            requester: [],
            requesterId: '',
            isAdmin: false,
            partnerList: [],
            isPartner: false
        }
    }
    checkParams = () => {
        var params = this.props.navigation.state.params;
        console.log(params)
        if (params) {
            if (params.photoId) {
                this.setState({
                    photoId: params.photoId,
                    curUser: params.curUser

                })
                this.fetchComments(params.photoId);
                this.fetchPartner(params.photoId)
                this.fetchRequester(params.photoId)
            }
        }
    }
    addCommentToList = (comments_list, data, comment) => {
        var that = this;
        var commentObj = data[comment]

        database.ref('user').child(commentObj.author).child('username').once('value').then((snapshot) => {
            const exist = (snapshot.val() !== null)
            if (exist) dota = snapshot.val()
            comments_list.push({
                id: comment,
                comment: commentObj.comment,
                posted: this.timeConverter(commentObj.posted),
                author: commentObj.commenter,
                authorId: commentObj.author
            })
            var newPhotoFeed = comments_list.slice(0);
            newPhotoFeed.sort(function (a, b) {
                return b.length - a.length;
            });
            that.setState({
                comments_list: newPhotoFeed
            });
            console.log('comments_list ', comments_list)
            that.setState({
                refresh: false,
                loading: false
            })

        })
    }

    fetchComments = (photoId) => {
        var that = this

        database.ref('photos').child(photoId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val()
                that.setState({
                    photo: data.url,
                    ownerId: data.author,
                    postName: data.postName,
                    des: data.caption,
                    price: data.price,
                    partnerId: data.partner
                })
            }
        }).catch(error => console.log(error))


        database.ref('comments').child(photoId).orderByChild('posted').once('value').then(function (snapshot) {

            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val()
                var comments_list = that.state.comments_list
                for (var comment in data) {
                    that.addCommentToList(comments_list, data, comment)
                }
            }
        }).catch(error => console.log(error))


    }
    pluralCheck = (s) => {
        if (s == 1) {
            return 'ago';
        } else {
            return 's ago'
        }
    }

    timeConverter = (timestamp) => {
        var a = new Date(timestamp * 1000);
        var secounds = Math.floor((new Date() - a) / 1000)
        var interval = Math.floor(secounds / 31536000)
        if (interval > 1) {
            return interval + ' year' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 2592000)
        if (interval > 1) {
            return interval + ' month' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 86400)
        if (interval > 1) {
            return interval + ' day' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 3600)
        if (interval > 1) {
            return interval + ' hour' + this.pluralCheck(interval);
        }
        interval = Math.floor(secounds / 60)
        if (interval > 1) {
            return interval + ' minute' + this.pluralCheck(interval);
        }
        return Math.floor(secounds) + ' second' + this.pluralCheck(interval)
    }
    s4 = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    uniqueId = () => {
        return (
            this.s4() + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4() + "-" + this.s4()
        );
    };
    componentDidMount = () => {
        var that = this;
        this.setState({
            loggedin: false,
            comments_list: [],
            photoId: '',
            photo: '',
            ownerId: '',
            owner: false,
            requester: [],
            requesterId: '',
            isAdmin: false
        })
        f.auth().onAuthStateChanged(function (user) {
            if (user) {

                that.fetchAdmin(user.uid)


            } else {
                // that.setState({
                //     loggedin: false
                // })
            }
        })
        this.checkParams()
    }
    componentWillReceiveProps = (nextState) => {
        if (nextState.comments_list != this.state.comments_list) {
            this.reloadCommentList();
        }
    }
    componentWillUnmount = () => {
        this.setState = [''];
    }
    loadNew = () => {
        this.setState({
            refresh: true
        });
        this.setState({
            quest: [6, 7, 8, 9, 10],
            refresh: false
        })
    }
    postComment = () => {
        var comment = this.state.comment
        if (comment != '') {
            var imageId = this.state.photoId
            var userId = f.auth().currentUser.uid
            var username = f.auth().currentUser.displayName
            var commentId = this.uniqueId();
            var datetime = Date.now()
            var timestamp = Math.floor(datetime / 1000)

            this.setState({
                comment: ''
            })
            var commentObj = {
                posted: timestamp,
                commenter: username,
                author: userId,
                comment: comment
            }
            database.ref('/comments/' + imageId + '/' + commentId).set(commentObj)
            this.reloadCommentList();
        } else {
            alert('Please enter comment')
        }
    }

    fetchAdmin = (userId) => {
        var that = this
        database.ref('user').child(userId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) data = snapshot.val();
            if (data.role == 'admin') {
                that.setState({
                    isAdmin: true
                })
            }
        }).catch(error => console.log(error))
    }
    fetchPartner = (photoId) => {
        database.ref('photos').child(photoId).child('partner').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val()
                var partnerList = this.state.partnerList
                for (var partner in data) {
                    this.findPartner(partnerList, data, partner)
                }
            }
            console.log("partnerList :", this.state.isPartner)
        }).catch(error => console.log(error));
    }
    fetchRequester = (photoId) => {
        database.ref('photos').child(photoId).child('requester').once('value').then((snapshot) => {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val()
                // var requester = this.state.requester
                for (var partner in data) {
                    console.log("Requester :", partner)
                    if (data[partner] == this.state.curUser)
                        this.setState({
                            isRequester: true
                        })
                }

            }
            console.log("partnerList :", this.state.isPartner)
        }).catch(error => console.log(error));
    }

    findPartner = (partnerList, data, partner) => {
        var partnerObj = data[partner]

        console.log("partner in loop", partnerObj)
        if (partnerObj == this.state.curUser) {
            this.setState({
                isPartner: true
            })
        }
    }
    findRequester = (requester, data, partner) => {
        var partnerObj = data[partner]

        console.log("partner in loop", partnerObj)
        if (partnerObj == this.state.curUser) {
            this.setState({
                isRequester: true
            })
        }
    }


    reloadCommentList = () => {
        this.setState({
            comments_list: [],
        })
        this.fetchComments(this.state.photoId)
    }

    requestJob = () => {
        var requestId = this.uniqueId()
        database.ref('/photos/' + this.state.photoId + '/requester/' + requestId).set(this.state.curUser).then(this.componentDidMount())
        console.log("this.state.photoId ", this.state.photoId)
        alert('Request was send ')

    }

    deletePost = () => {
        console.log("Current user", this.state.curUser)
        console.log("Photo Id", this.state.photoId)
        Alert.alert(
            'Delete',
            'Are you Sure to delete?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Sure', onPress: () => database.ref('/photos/' + this.state.photoId).remove().then(database.ref('/user/' + this.state.curUser + '/photos/' + this.state.photoId).remove()).then(() => this.props.navigation.navigate('Feed')) },
            ],
            { cancelable: false }
        );
        // database.ref('/photos/' + this.state.photoId).remove
    }
    renderViewMore(onPress) {
        return (
            <Text style={{ color: 'blue' }} onPress={onPress}>View more</Text>
        )
    }
    renderViewLess(onPress) {
        return (
            <Text style={{ color: 'blue' }} onPress={onPress}>View less</Text>
        )
    }
    componentWillUnmount() {
        this.state = [''];
        // this.componentDidMount();
    }
    render() {
        // console.log("User id and owner",this.state.curUser, this.state.ownerId)
        return (
            <ScrollView >
                <View style={{ flex: 1 }}>

                    <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', borderBottomWidth: 0.5, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ width: '80%', padding: 5, flexDirection: 'row', justifyContent: 'space-between', marginRight: 10 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Text>Back</Text>
                            </TouchableOpacity>
                            <Text>Posts</Text>

                            {this.state.curUser == this.state.ownerId ? (
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('EditPost', { photoId: this.state.photoId, currentUser: this.state.ownerId })}>
                                    <Text>Edit</Text>
                                </TouchableOpacity>
                            ) : <Text></Text>}
                        </View>
                    </View>


                    <View>
                        <Image source={{ uri: this.state.photo }} style={{ resizeMode: 'cover', width: '100%', height: 275 }} />
                        <Text style={{ fontWeight: 'bold' }}>Topic  :</Text>
                        <ViewMoreText
                            numberOfLines={3}
                            renderViewMore={this.renderViewMore}
                            renderViewLess={this.renderViewLess}
                        >
                            <Text style={{ textAlign: 'center' }}>{this.state.postName}</Text>
                        </ViewMoreText>
                        <Text style={{ fontWeight: 'bold' }}>Description  :</Text>
                        <ViewMoreText
                            numberOfLines={3}
                            renderViewMore={this.renderViewMore}
                            renderViewLess={this.renderViewLess}
                        >
                            <Text>{this.state.des}</Text>
                        </ViewMoreText>
                        <Text style={{ fontWeight: 'bold' }}>Price  :</Text>
                        <Text style={{ textAlign: 'center' }}>{this.state.price}</Text>
                        {this.state.curUser == this.state.ownerId || this.state.isPartner ? (
                            <View>
                                {this.state.curUser == this.state.ownerId ? (
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate('CheckRequester', { photoId: this.state.photoId, postName: this.state.postName })}
                                            style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Check request</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => this.deletePost()}
                                            style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Delete Post</Text>
                                        </TouchableOpacity>
                                    </View>
                                ) : (null)}
                                {this.state.isPartner ? (
                                    <View>
                                          <TouchableOpacity 
                                            onPress={() => this.props.navigation.navigate('Chat', { photoId: this.state.photoId, curUser: this.state.curUser })}
                                            style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Chat</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate('Confirm', { photoId: this.state.photoId })}
                                            style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>Send bill</Text>
                                        </TouchableOpacity>
                                    </View>
                                ) : null}
                            </View>
                        ) : (
                                <View style={{ justifyContent: 'space-between' }}>
                                    {this.state.isRequester ? (null) : (
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => this.requestJob()}
                                                style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                                <Text style={{ textAlign: 'center', color: 'white' }}>Send request</Text>
                                            </TouchableOpacity>
                                        </View>)}

                                    <View>
                                        {this.state.isAdmin ? (
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => this.props.navigation.navigate('Admincheckbill', { photoId: this.state.photoId })}
                                                    style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                                    <Text style={{ textAlign: 'center', color: 'white' }}>Check Bill</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress={() => this.deletePost()}
                                                    style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                                    <Text style={{ textAlign: 'center', color: 'white' }}>Delete Post</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ) : (
                                          null
                                            )}
                                    </View>
                                </View>
                            )}


                    </View>
                    <View >
                        <FlatList
                            data={this.state.comments_list}
                            refreshing={this.state.refresh}
                            keyExtractor={(index) => index.toString()}
                            style={{ flex: 1, backgroundColor: '#eee' }}
                            initialNumToRender={10}
                            renderItem={({ item }) => (
                                <View style={{ width: '100%', overflow: 'hidden', marginBottom: 5, justifyContent: 'space-between', borderBottomWidth: 1, borderColor: 'grey' }}>
                                    <View style={{ padding: 5, width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text>{item.posted}</Text>
                                        <TouchableOpacity onPress={() => this.props.navigation('User', { userId: item.authorId })}>
                                            <Text>{item.author}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ padding: 5 }}>
                                        <Text>{item.comment}</Text>
                                    </View>
                                </View>
                            )}
                        />
                    </View>





                    <View>



                        <Text>Comments</Text>
                        <View>
                            <TextInput
                                editable={true}
                                placeholder={'enter your comment'}
                                clearButtonMode="always"
                                onChangeText={(text) => this.setState({ comment: text })}
                                style={{ marginVertical: 10, height: 50, padding: 5, borderColor: 'black', borderRadius: 5, backgroundColor: 'white', color: 'black' }}
                            />
                            <TouchableOpacity
                                onPress={() => this.postComment()}
                                style={{ marginTop: 10, marginVertical: 20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D', marginHorizontal: 100 }}>
                                <Text style={{ textAlign: 'center', color: 'white' }}>Comment</Text>
                            </TouchableOpacity>

                            <KeyboardSpacer />
                        </View>
                        <KeyboardSpacer />

                    </View>
                    <KeyboardSpacer />
                </View>
                <KeyboardSpacer />
            </ScrollView>
        )
    }
}
Comments.shared = new Comments()
export default Comments;