import React from 'react';
import { TouchableOpacity, FlatList, Styesheet, Text, View, Image, TextInput, ImageBackground } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';



class admincheckbill extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedin: true,
            loading: true,
            curUser: '',
            bill_list: [],
            anyBill: false,
            photoId: ''

        }
    }

    componentDidMount = () => {
        var that = this
        that.checkParams()
    }

    checkParams = () => {
        var that = this
        var params = this.props.navigation.state.params;
        this.setState({
            photoId: params.photoId
        })
        this.photoList(params.photoId)
    }

    photoList = (photoId) => {
        var that = this
        this.setState({
            refresh: true,
            bill_list: [],
        })
        database.ref('photos').child(photoId).child('checkbill').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val()
                var bill_list = that.state.bill_list
                for (var bill in data) {
                    var billObj = data[bill]
                    bill_list.push({
                        id: bill,
                        author: billObj.author,
                        posted: billObj.posted,
                        url: billObj.url
                    })
                }
                var newBillList = bill_list.slice(0)
                newBillList.sort(function (a, b) {
                    return b.length - a.length;
                })
                that.setState({
                    bill_list: newBillList,
                    anyBill: true,
                    refresh: false,
                    loading: false,
                })
            } else {
                that.setState({ anyBill: false })
            }
        }).catch(error => console.log(error))

        database.ref('photos').child(photoId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) {
                data = snapshot.val()
                that.setState({
                    price: data.price,
                })
            }
        }).catch(error => console.log(error))
    }

    promoteRank = (userId, itemId) => {
        price = this.state.price;
        console.log("userId in rank", userId)
        var that = this
        Alert.alert(
            'Delete post',
            'Delete this post?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Accept', onPress: () => database.ref('/photos/' + this.state.photoId + '/checkbill/' + itemId).remove().then(this.loadNew
                    )
                },
            ],
            { cancelable: false }
        );

        database.ref('user').child(userId).child('exp').once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)

            if (exists) {
                data = snapshot.val();
                that.setState({
                    exp: data + (price / 10)
                })
                database.ref('user').child(userId).child('exp').set(that.state.exp)
                console.log("data is ?", that.state.exp)
            } else {
                console.log("NO data")
            }

        }).catch(error => console.log(error))
    }

    deleteBill = (itemId) => {
        Alert.alert(
            'Delete post',
            'Delete this post?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Accept', onPress: () => database.ref('/photos/' + this.state.photoId + '/checkbill/' + itemId).remove().then(this.loadNew
                    )
                },
            ],
            { cancelable: false }
        );
    }

    loadNew = () => {
        this.photoList(this.state.photoId)
        this.setState({
            refresh: true,
            requester: []
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                {this.state.loading ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 80 }}>
                        {this.state.empty == true ? (
                            <View><Text>No request found</Text></View>
                        ) : (
                                <View>
                                    <Text>Loading... </Text>
                                </View>
                            )}
                    </View>
                ) : (
                        <View style={{ flex: 1, justifyContent: 'center', marginTop: 100 }}>
                            {this.state.anyBill ? (
                                <FlatList refreshing={this.state.refresh}
                                    onRefresh={this.loadNew}
                                    data={this.state.bill_list}
                                    keyExtractor={(item, index) => index.toString()}
                                    style={{ flex: 1, backgroundColor: '#eee' }}
                                    renderItem={({ item, index }) => (
                                        <View key={index}>
                                            <View>
                                                <Image source={{ uri: item.url }}
                                                    style={{ resizeMode: 'cover', width: '100%', height: 275 }} />
                                            </View>
                                            <View style={{ justifyContent: 'center', justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text>{item.author}</Text>
                                                <TouchableOpacity
                                                    onPress={() => this.promoteRank(item.author, item.id)}>
                                                    <Text style={{ marginLeft: 30 }}>Accept</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress={() => this.deleteBill(item.id)}>
                                                    <Text>Decline</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    )} />
                            ) : (
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 100 }}>
                                        <Text>No request</Text>
                                    </View>
                                )}
                        </View>
                    )}
            </View>
        )
    }
}
export default admincheckbill;
