import React from 'react';
import { FlatList, Styesheet, Text, View, Image, TextInput, Keyboard, ActivityIndicator } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import Icon from 'react-native-vector-icons/Ionicons'
import * as animatable from 'react-native-animatable'
import { ListItem, SearchBar } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ViewMoreText from 'react-native-view-more-text';


class search extends React.Component {

  serchState = {
    searchBarFocus: false
  }

  constructor(props) {
    super(props);
    this.state = {
      loggedin: false,
      refresh: false,
      loading: false,
      data: [],
      error: null,
      spamPhoto: [],
      dataSource: '',
      description: '',
      wordList: '',
      respondInState: ''
    }
    this.arrayholder = [];

  }

  componentDidMount = () => {
    this.checkAuthState()
    //this.fetchPost()
    this.getSuggestedWord()
    this.KeyboardDidShow = Keyboard.addListener('KeyboarDidShow', this.KeyboardDidShow)
    this.KeyboardWillShow = Keyboard.addListener('KeyboardWillShow', this.KeyboardWillShow)
    this.KeyboardWillHide = Keyboard.addListener('KeyboardWillHide', this.KeyboardWillHide)
    this.makeRemoteRequest();
  }
  KeyboardDidShow = () => {
    this.serchState({ searchBarFocus: true })
  }
  KeyboardWillShow = () => {
    this.serchState({ searchBarFocus: true })
  }
  KeyboardWillHide = () => {
    this.serchState({ searchBarFocus: false })
  }
  checkAuthState() {
    f.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          curUser: user.uid
        })
      }

    })
  }

  fetchPost = (photoId = '') => {
    this.setState({
      refresh: true,
      loading:true,
      photo_feed: []
    })
    if (photoId != '') {
      this.setState({
        loadList: true,
        refresh: false,
        loading:true
      })
      console.log('photoId id exisst')
      database.ref('photos').child(photoId).once('value').then((snapshot) => {
        const exists = (snapshot.val() !== null);
        if (exists) data = snapshot.val();
        var spamPhotoList = this.state.spamPhoto;
        for (var photo in data) {
          this.addPostToFlatList(spamPhotoList, data, photo)
        }
        this.setState({
          loading: false
        })
        console.log('photolist',spamPhotoList)

      }).catch(error => console.log(error));
    } else {
      console.log('in else')
      database.ref('photos').once('value').then((snapshot) => {
        const exists = (snapshot.val() !== null);
        console.log('snap',snapshot.val())
        if (exists) data = snapshot.val();
        var spamPhotoList = this.state.spamPhoto;
        for (var photo in data) {
          this.addPostToFlatList(spamPhotoList, data, photo)
        }
        this.setState({
          loadList: true,
          refresh: false
        })

      }).catch(error => console.log(error));
    }
  }

  addPostToFlatList = (spamPhotoList, data, photo) => {
    var photoObj = data[photo]
    spamPhotoList.push({
      caption: photoObj.postName,//ทำไมมันต้องสลับกันว่ะ
      postName: photoObj.caption,
      id: photo,
      price: photoObj.price,
      url: photoObj.url,
    })
    this.setState({
      spamPhoto: spamPhotoList
    })

  }

  makeRemoteRequest = () => {
    const url = `https://randomuser.me/api/?&results=20`;
    this.setState({ loading: true });
    //console.log(url)
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: this.state.spamPhoto,
          error: res.error || null,
          loading: false,
        });

        this.arrayholder = this.state.spamPhoto;//res.results
        //console.log("In make remote", this.arrayholder)

      })
      .catch(error => {
        this.setState({ error});
      });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  renderHeader = () => {
    return (
      <SearchBar
        placeholder="Type Here..."
        lightTheme
        round
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
        value={this.state.value}
      />
    );
  };
  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.caption.toUpperCase()} ${item.postName.toUpperCase()}`;
      // const itemData = `${item.name.title.toUpperCase()} ${item.name.first.toUpperCase()} ${item.name.last.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };

  getSuggestedWord = () => {
    return fetch('http://huntelastic.herokuapp.com/search=samsunng').then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          respondInState: responseJson
        })
        console.log('responseJson in state', this.state.respondInState);
        var wordList = this.state.wordList
        var counter = 0
        for (inwordList in responseJson) {
          this.addtoWordList(this.state.respondInState[counter], responseJson, wordList,counter)
          counter++
        }
        return responseJson

      }).catch((error) => {
        console.error(error);
      });
  }
  addtoWordList = (inwordList, responseJson, wordList,counter) => {
    console.log('wordObj', inwordList, 'counter', counter)
    this.fetchPost(inwordList)

  }

  renderViewMore(onPress) {
    return (
      <Text style={{ color: 'blue' }} onPress={onPress}>View more</Text>
    )
  }
  renderViewLess(onPress) {
    return (
      <Text style={{ color: 'blue' }} onPress={onPress}>View less</Text>
    )
  }
  componentWillUnmount() {
    this.state = [''];
    // this.componentDidMount();
  }
  render() {
    // console.log("data is :",this.state.dataSource)
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View>
        <View style={{ height: 70, paddingTop: 30, backgroundColor: '#F55E61', borderColor: 'lightgrey', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <View></View>
          <Text>Search </Text>
          <View></View>
        </View>
        <SearchBar
          placeholder="Type Here..."
          lightTheme
          round
          method="get"
          onChangeText={text => this.searchFilterFunction(text)}
          autoCorrect={false}
          value={this.state.value}
        />
        <View>
        </View>
        <View>
          <FlatList
            data={this.state.data}
            renderItem={({ item }) => (
              <View>
                <View>
                  <Image source={{ uri: item.url }}
                    style={{ marginTop: 5, resizeMode: 'cover', width: 50, height: 50 }} />
                </View>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('Comment', { photoId: item.id, curUser: this.state.curUser })}>
                  <ViewMoreText
                    numberOfLines={3}
                    renderViewMore={this.renderViewMore}
                    renderViewLess={this.renderViewLess}
                  ><Text style={{ marginTop: 10 }}>Post name: {item.caption}</Text>
                  </ViewMoreText>
                </TouchableOpacity>
                <ViewMoreText
                  numberOfLines={3}
                  renderViewMore={this.renderViewMore}
                  renderViewLess={this.renderViewLess}
                >
                  <Text>{item.postName}</Text>
                </ViewMoreText>


              </View>
            )}
            // keyExtractor={item => item.price}
            ItemSeparatorComponent={this.renderSeparator}
          // ListHeaderComponent={this.renderHeader}
          />
        </View>
      </View>
    );

  }
}
export default search;