import React from 'react'
import { TouchableOpacity, FlatList, Styesheet, Text, View, Image, TextInput } from 'react-native'
import { f, auth, database, storage } from '../config/firebaseConfig.js';
import { grey } from 'ansi-colors';

class editpost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,

        }
    }
    checkParams = () => {
        var that = this;
        var params = this.props.navigation.state.params;
        console.log("in edit: ", params.photoId)
        f.auth().onAuthStateChanged(function (user) {
            if (user) {
                that.setState({
                    curUser: user.uid
                })
                console.log("curUser: ", that.state.curUser)
            }

        })
        if (params) {
            if (params.photoId) {
                this.setState({
                    photoId: params.photoId
                })
                this.fetchPhoto(params.photoId);
            }
        }
    }
    fetchPhoto = (photoId) => {
        var that = this;
        database.ref('photos').child(photoId).once('value').then(function (snapshot) {
            const exists = (snapshot.val() !== null)
            if (exists) data = snapshot.val();
            that.setState({
                author: data.userId,
                caption: data.caption,
                price: data.price,
                postName: data.postName,
                url: data.url
            })
            console.log(that.state.author)

        }).catch(error => console.log(error))
    }




    componentDidMount = () => {
        this.checkParams();
    }

  

    savePost = () => {
        var postName = this.state.postName
        var caption = this.state.caption
        var price = this.state.price
        if(postName !== ''&&caption !== ''&&price !== ''){
            database.ref('photos').child(this.state.photoId).child('postName').set(postName)
            database.ref('user').child(this.state.curUser).child('photos').child(this.state.photoId).child('postName').set(postName)
     
            database.ref('photos').child(this.state.photoId).child('caption').set(caption)
            database.ref('user').child(this.state.curUser).child('photos').child(this.state.photoId).child('caption').set(caption)
   
            database.ref('photos').child(this.state.photoId).child('price').set(price)
            database.ref('user').child(this.state.curUser).child('photos').child(this.state.photoId).child('price').set(price)
            alert('Saved Successfully!')
            this.props.navigation.navigate('Feed', this.setState({loaded:false}))
        }else{
            alert('Please fill all form')
        }
    }

    render() {
        return (
            <View>
                <View style={{ height: 70, paddingTop: 30, backgroundColor: '#ff999c', borderColor: 'lightgrey', borderBottomWidth: 0.5, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{this.state.postName}</Text>
                </View>
                <TouchableOpacity
                    style={{ marginTop: 10, marginRight: 160,marginLeft:20, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D' }}>
                    <Text style={{ textAlign: 'center', color: 'white' }}>Upload picture</Text>
                </TouchableOpacity>
                <Text style={{ marginTop: 5 }}>Topic:</Text>
                <TextInput
                    editable={true}
                    placeholder={'Topic'}
                    value={this.state.postName}
                    maxLength={20}
                    multipleLine={4}
                    numberOfLine={4}
                    onChangeText={(text) => this.setState({ postName: text })}
                    style={{ marginVertical: 10, height: 50, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
                />
                <Text style={{ marginTop: 5 }}>Description</Text>
                <TextInput
                    editable={true}
                    placeholder={'Description'}
                    value={this.state.caption}
                    maxLength={150}
                    multipleLine={4}
                    numberOfLine={4}
                    onChangeText={(text) => this.setState({ caption: text })}
                    style={{ marginVertical: 10, height: 100, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
                />
                <Text style={{ marginTop: 5 }}>Price</Text>

                <TextInput
                    editable={true}
                    placeholder={'Price'}
                    value={this.state.price}
                    maxLength={16}
                    multipleLine={4}
                    numberOfLine={4}
                    keyboardType={'numeric'}
                    onChangeText={(text) => this.setState({ price: text })}
                    style={{ marginVertical: 10, height: 50, padding: 5, borderColor: 'grey', borderWidth: 1, borderRadius: 3, backgroundColor: 'white', color: 'black' }}
                />

                <TouchableOpacity
                    onPress={() => this.savePost()}
                    style={{ marginTop: 10, marginHorizontal: 40, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D' }}>
                    <Text style={{ textAlign: 'center', color: 'white' }}>Save</Text>
                </TouchableOpacity>
                <TouchableOpacity
                      onPress={() => this.props.navigation.goBack()}
                    style={{ marginTop: 10, marginHorizontal: 40, paddingVertical: 15, borderRadius: 20, borderColor: grey, borderWidth: 1.5, backgroundColor: '#DB393D' }}>
                    <Text style={{ textAlign: 'center', color: 'white' }}>Cancel</Text>
                </TouchableOpacity>
            </View>

        )
    }

};

export default editpost;