import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  createStackNavigator,
  createAppContainer,
  createBottomTabNavigator
} from 'react-navigation';
import feed from '../screens/feed'
import search from '../screens/search.js'
import profile from '../screens/profile.js'
import Icon from 'react-native-vector-icons/Ionicons';
import activity from '../screens/activity.js'
import comments from '../screens/comments.js'
import userProfile from '../screens/userProfile.js'
import upload from '../screens/upload.js'
import editProfile from '../screens/editProfile.js'
import editPost from '../screens/editPost.js'
import confirm from '../screens/confirm.js'
import chat from '../screens/chat'
import admincheckbill from '../screens/admincheckbill.js'
import Login from '../screens/authen/Login.js'
import searchElas from '../screens/searchElas.js'
import userActivity from '../screens/userActivity.js'
import checkRequester from '../screens/checkRequester.js'





import { f, auth, database, storage } from '../config/firebaseConfig';


const TabStack = createBottomTabNavigator(
  {
    Feed: {
      screen: feed,
      navigationOptions: {
        tabBarLabel: 'Feed',
        tabBarIcon: () => (<Icon name="md-list-box" size={30} color="#F55E61" />)
      }
    },
    SearchElas: {
      screen: searchElas,
      navigationOptions: {
        tabBarLabel: 'Search',
        tabBarIcon: () => (<Icon name="md-search" size={30} color="#F55E61" />)
      }
    },
    Upload: { screen: upload,
      navigationOptions: {
        tabBarLabel: 'Upload',
        tabBarIcon: () => (<Icon name="ios-cloud-upload" size={30} color="#F55E61" />)
      } },
    Activity: { screen: activity,
      navigationOptions: {
        tabBarLabel: 'Activity',
        tabBarIcon: () => (<Icon name="md-folder-open" size={30} color="#F55E61" />)
      } },
    Profile: { screen: profile,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: () => (<Icon name="md-person" size={30} color="#F55E61" />)
      } },


  }, {
  tabBarOptions: {
    showIcon: true,
    showLabel: true,
  }
}

)
const buttonStack = createStackNavigator(
  {
    Home: { screen: TabStack },

    User: { screen: userProfile },
    Comment: { screen: comments },
    Upload: { screen: upload },
    EditProfile: { screen: editProfile },
    Activity: { screen: activity },
    EditPost: { screen: editPost },
    Confirm: {screen: confirm},
    Admincheckbill: {screen: admincheckbill},
    Login : {screen: Login},
    Chat: {screen: chat },
    CheckRequester: {screen: checkRequester}
  }
  ,
  {
    initialRouteName: 'Home',
    mode: 'modal',
    headerMode: 'none'
  }
)
const Navigat = createAppContainer(buttonStack)

export default class MainTabNavigator extends React.Component {

  constructor(props) {
    super(props);
    // this.login();
  }


  render() {
    return (
      <Navigat />

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
